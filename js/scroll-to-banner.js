export const scrollToBanner = () => {
    window.addEventListener('scroll', function () {
        const element = document.getElementById('aside-btn');
        const banner = document.getElementById('sticky-banner');
        const elementHeight = element.offsetTop + element.offsetHeight;
        if (window.pageYOffset >= elementHeight) {
            banner.classList.remove("display-none");
        } else {
            banner.classList.add("display-none");
        }
    });
}